# bees

Best-Effort Extent-Same, a btrfs deduplication agent. See here: https://github.com/Zygo/bees

> This image is tested for use with **podman** on **Linux** hosts with
> **x86_64** arch


## What is bees?

Bees is a best-effort extent-same btrfs deduplication agent.

See [the project website](https://github.com/Zygo/bees)


## How to use this image

This image is meant to be a one-to-one replacement of a natively compiled and
installed `bees`. To get started, you will have to get the UUID of the btrfs
filesystem you want to deduplicateand create a configuration file for it.
You can get the configuration template from within the container:

```bash
$ podman run --rm -it --entrypoint cat registry.gitlab.com/c8160/bees \
    /disk.conf | tr -d '\r' > disk.conf
```

This will create a `disk.conf` in your current directory. Open up the file and
fill in:

- the UUID of the btrfs filesystem to deduplicate (Get it with `lsblk -f`,
  `blkid`, etc.)
- the size of the hashtable ([Refer to the docs][1])

Now run the container (Replace `$UUID` with your real UUID):

```bash
$ sudo podman run --rm -it --privileged -v /dev:/dev -v "./disk.conf:/etc/bees/disk.conf:ro" registry.gitlab.com/c8160/bees $UUID
```

An example invocation **using stronger isolation and fewer privileges** (thanks
to @thjderjktyrjkt):

```bash
# Fill this in first
$ UUID=xxx
$ sudo podman run --rm -it --name "bees-$UUID" --replace \
    --cap-drop all \
    --cap-add cap_dac_override,cap_dac_read_search,cap_fowner,cap_sys_admin \
    --security-opt label=disable \
    --network none \
    --log-driver none \
    --label io.containers.autoupdate=registry \
    --tz local \
    --device "/dev/disk/by-uuid/$UUID:/dev/disk/by-uuid/$UUID" \
    -v "/etc/bees/$UUID.conf:/etc/bees/$UUID.conf:ro" \
    registry.gitlab.com/c8160/bees \
    --verbose=3 --scan-mode=1 "$UUID"
```


## Getting help

First you may want to refer to the `help.md` contained in this repository or
the containers rootfs. If you're still stuck or found an issue with the
container in particular, feel free to open an issue on Gitlab:
https://gitlab.com/c8160/bees/-/issues


[1]: https://github.com/Zygo/bees/blob/master/docs/config.md
