# Build bees first
FROM fedora:39 AS BUILDER

ENV VERSION=v0.10
WORKDIR /root
RUN dnf -y install gcc-c++ make btrfs-progs util-linux-core git
RUN git clone https://github.com/Zygo/bees.git --branch $VERSION
WORKDIR /root/bees

# Fix weird DOS line endings messing up poor bash
RUN make; chmod +x scripts/beesd

# This creates the actual finished container
FROM registry.fedoraproject.org/fedora-minimal:39

ENV NAME=bees ARCH=x86_64
LABEL   name="$NAME" \
        license="GNU GPLv3" \
        architecture="$ARCH" \
        run="podman run --privileged -v /dev:/dev -v BEES_CONFIG_DIR:/etc/bees:ro IMAGE UUID" \
        summary="Deduplicates btrfs filesystems" \
        maintainer="Andreas Hartmann <hartan@7x.de>" \
        url="https://gitlab.com/c8160/bees"

# Move bees to a new container
COPY --from=BUILDER /root/bees/bin/bees /usr/lib/bees/bees
COPY --from=BUILDER /root/bees/scripts/beesd /usr/sbin/beesd
COPY --from=BUILDER /root/bees/scripts/beesd.conf.sample /disk.conf
# Copy documentation into container
COPY help.md /
# Install bees runtime dependencies
RUN microdnf -y install btrfs-progs util-linux; \
    rm -rf /var/cache

ENTRYPOINT [ "/usr/sbin/beesd" ]
