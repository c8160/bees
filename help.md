bees image, for deduplicating btrfs filesystems.

Packages the [bees Github project](https://github.com/Zygo/bees), in particular
the `bees` executable and the `beesd` wrapper-script which is also the
entrypoint of this container.

Volumes:
bees-config: Mapped to /etc/bees in the container, contains configuration files
  per btrfs filesystem to deduplicate with bees. Bees can not run without this.
  Can be mapped read-only. See below for an example configuration file.

Documentation:
For the underlying bees project, see https://github.com/Zygo/bees/tree/master/docs
For this container, see https://gitlab.com/c8160/bees

Requirements:
Works only on btrfs filesystems. Devices to deduplicate must be discoverable
under /dev/disk/by-uuid/ with the UUID given to the container as argument. The
container must be launched as root, with privileges and without SELinux
labeling, as otherwise it cannot access and mount the filesystems for
deduplication.

Configuration:
Sample disk.conf is included in the image. Modify the contents to suit your
requirements, keep the information from the documentation in mind [1][1]. To
dump the configuration to a file on your host, run:

```bash
$ podman run --rm -it --entrypoint cat IMAGE /disk.conf > disk.conf
```

The `beesd` wrapper that is the default entrypoint forwards all arguments it
doesn't know to `bees`.


[1]: https://github.com/Zygo/bees/blob/master/docs/config.md
